from django import forms
#from contact_form.forms import ContactForm, AkismetContactForm
from antispam.honeypot.forms import HoneypotField
from antispam import akismet
from django.template import loader
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from portfolio.settings import production
from django.contrib import messages


class ContactForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput(), error_messages={'required': 'Please enter your name.'})
    email = forms.EmailField(widget=forms.EmailInput(), error_messages={'required': 'Type in your email.'})
    body = forms.CharField(widget=forms.Textarea(attrs={'style': 'resize: vertical; max-height: 40px;'}), error_messages={'required': 'Type in your message.'})
    spam_honeypot_field = HoneypotField()


class AkismetContactForm(ContactForm):
    
    def __init__(self, *args,**kwargs):
        self.request = kwargs.pop('request', None)

        super().__init__(*args, **kwargs)

    def clean(self):
        if self.request and akismet.check(
            request=akismet.Request.from_django_request(self.request),
            comment=akismet.Comment(
                content=self.cleaned_data['body'],
                type='comment',

                author=akismet.Author(
                    name=self.cleaned_data['name'],
                    email=self.cleaned_data['email']
                )
            )
        ):
            raise ValidationError('Spam detected', code='spam-protection')

        return super().clean()

#error_messages={'required': 'Please enter your name.'}
    # def clean_body(self):
    #     if 'body' in self.cleaned_data:
    #         from akismet import Akismet
    #         akismet_api = Akismet(key=settings.AKISMET_API_KEY,blog_url= settings.AKISMET_BLOG_URL)
    #         akismet_kwargs = {
    #             'user_ip': self.request.META['REMOTE_ADDR'],
    #             'user_agent': self.request.META.get('HTTP_USER_AGENT'),
    #             'comment_author': self.cleaned_data.get('name'),
    #             'comment_author_email': self.cleaned_data.get('email'),
    #             'comment_content': self.cleaned_data['body'],
    #             'comment_type': 'contact-form',
    #         }
    #         if akismet_api.comment_check(**akismet_kwargs):
    #             raise forms.ValidationError(
    #                 self.SPAM_MESSAGE
    #             )
    #         return self.cleaned_data['body']



    #         #error_messages={'required': 'Please enter your name'}