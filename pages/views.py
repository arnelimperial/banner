from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from .forms import ContactForm, AkismetContactForm
from django.views.generic.edit import FormView
#from django.urls import reverse
from django.core.mail import send_mail, BadHeaderError, send_mass_mail
from django.http import HttpResponse, HttpResponseRedirect
from django.conf import settings
import datetime
from datetime import date
from django.contrib import messages




class HomeView(TemplateView):
    template_name = 'pages/main.html'

    def get_context_data(self, *args, **kwargs):
        context = super(HomeView, self).get_context_data(*args, **kwargs)
        month_object = datetime.datetime.now()
        month = month_object.strftime("%B")
        today = date.today()
        #context['title'] = "Arnel Imperial | arnelimperial.com({})".format(month, today.year)
        context['title'] = "Arnel Imperial | arnelimperial.com"
        context['description'] = "Arnel Imperial\'s internet site."
        context['gtag'] = 'XGkUfqt3z01McIWECdHOckgGdTn7fgRAYZ4R8zsFowU'
        return context

class AboutView(TemplateView):

    template_name = 'pages/about.html'

    def get_context_data(self, *args, **kwargs):
        context = super(AboutView, self).get_context_data(*args, **kwargs)
        context['title'] = 'About'
        context['description'] = "Arnel Imperial\'s internet site about page."
        return context



class InsightsView(TemplateView):

    template_name = 'pages/insights.html'

    def get_context_data(self, *args, **kwargs):
        context = super(InsightsView, self).get_context_data(*args, **kwargs)
        context['title'] = 'Insight'
        context['description'] = "Some of the thoughts that the author wnats to share in wwww."
        return context



class ContactView(FormView):
    template_name = 'contact_form/contact_form.html'
    form_class = AkismetContactForm

    def get(self, request):
        form = AkismetContactForm()
        title = 'Contact'
        return render(request, self.template_name, {'form': form, 'title': title})

    def post(self, request):
        form = AkismetContactForm(request.POST or None)
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            body = form.cleaned_data['body']
            email_from = settings.EMAIL_HOST_USER
            message = '{}\n\nSender E-mail: {}\nSender Name: {}'.format(body, email, name)
            message_for_sender = "Thanks for using the contact form from my website. Below is the copy of your message to me.\n\n\n{}".format(body)
            recipient_list = [settings.EMAIL_RECEIVER, ]
            subject = 'You have a message from [ {} | {} ]'.format(name, email)
            subject_for_sender = 'Copy of your email to Arnel Imperial'
            #send_mail(subject, message, email_from, recipient_list, fail_silently=False,)
            message_to_arnel = (subject, message, email_from, [recipient_list,])

            senders_copy = (subject_for_sender, message_for_sender, email_from, [email])
            send_mass_mail((message_to_arnel, senders_copy), fail_silently=False)

            return render(request, 'contact_form/contact_form_sent.html', {'form':form, 'name':name, 'email':email})
        
        else:
            #messages.warning(request, 'Please correct the error below.')
            return render(request, self.template_name, {'form': form})

   
    
               
class SendView(TemplateView):

    template_name = 'contact_form/contact_form_sent.html'

    def get_context_data(self, *args, **kwargs):
        context = super(SendView, self).get_context_data(*args, **kwargs)
        context['title'] = 'Mail Sent'
        return context



# form_class = ContactForm
#     template_name = 'contact_form/contact_form.html'
 
#     def form_valid(self, form):
#         form.save()
#         return super(AkismetContactForm, self).form_valid(form)
 
#     def get_form_kwargs(self):
#         # ContactForm instances require instantiation with an
#         # HttpRequest.
#         kwargs = super(AkismetContactForm, self).get_form_kwargs()
#         kwargs.update({'request': self.request})
#         return kwargs
 
#     def get_success_url(self):
#         # This is in a method instead of the success_url attribute
#         # because doing it as an attribute would involve a
#         # module-level call to reverse(), creating a circular
#         # dependency between the URLConf (which imports this module)
#         # and this module (which would need to access the URLConf to
#         # make the reverse() call).
#         return reverse('contact_form_sent')
#     # template_name = 'contact_form/contact_form.html'
#     # #Change to ContactForm class on local
#     # form_class =AkismetContactForm
#     # success_url = '/contact/sent'