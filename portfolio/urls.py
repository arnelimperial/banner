from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from portfolio.settings import WINDOWS, WINDOWSOTP
from django.views.generic import TemplateView
from django_otp.admin import OTPAdminSite
from django.contrib.auth.models import User
from django_otp.plugins.otp_totp.models import TOTPDevice


class OTPAdmin(OTPAdminSite):
    pass

admin_log = OTPAdmin(name='OTPAdmin')
admin_log.register(User)
admin_log.register(TOTPDevice)

urlpatterns = [
    path(WINDOWS, admin.site.urls),
    path(WINDOWSOTP, admin_log.urls),
    path('', include('pages.urls')),
    path('robots.txt/', TemplateView.as_view(template_name="robots.txt", content_type='text/plain')),
    path('sitemap.xml/', TemplateView.as_view(template_name="sitemap.xml", content_type='text/xml')),
    path('humans.txt/', TemplateView.as_view(template_name="humans.txt", content_type='text/plain')),
]

urlpatterns += staticfiles_urlpatterns()

