import os
from decouple import config, Csv
from environs import Env
from dotenv import load_dotenv, find_dotenv


env = Env()
env.read_env()
load_dotenv(find_dotenv())

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        'TEST': {
            'NAME': env.str("DB_NAME"),
        },

    }
}

# Heroku: Update database configuration from $DATABASE_URL.
import dj_database_url
db_from_env = dj_database_url.config(conn_max_age=500)
DATABASES['default'].update(db_from_env)

#Email SMTP server

DEFAULT_FROM_EMAIL = env.str("DEFAULT_FROM_EMAIL")

EMAIL_USE_TLS = env.bool("EMAIL_USE_TLS")
EMAIL_HOST = env.str("EMAIL_HOST")
EMAIL_HOST_USER = env.str("EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = env.str("EMAIL_HOST_PASSWORD")
EMAIL_PORT= env.int("EMAIL_PORT")

ADMINS = (
    ('Arnel Imperial', EMAIL_HOST_USER),  
)

MANAGERS = ADMINS

EMAIL_RECEIVER = config('EMAIL_RECEIVER')
