import os
#import django_heroku
from decouple import config, Csv
from environs import Env
from dotenv import load_dotenv, find_dotenv

env = Env()
env.read_env()
load_dotenv(find_dotenv())


# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql',
#         'AUTH_SOURCE': config('DB_NAME'),
#         'NAME': config('DB_NAME'),
#         'USER': config('DB_NAME'),
#         'PASSWORD': config('DB_PASSWORD'),
#         'HOST': config('DB_HOST'),
#         'PORT': config('DB_PORT', cast=int),
#     }
# }

# Akismet API keys
AKISMET_API_KEY = config('PYTHON_AKISMET_API_KEY')

AKISMET_BLOG_URL = config('PYTHON_AKISMET_BLOG_URL')

AKISMET_TEST_MODE = config('PYTHON_AKISMET_TEST_MODE')